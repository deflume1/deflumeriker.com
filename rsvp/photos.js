const knex = require('knex')(require('./knexfile'))

module.exports = {
  photos ({ filename }) {
    var query = knex('photos').insert({
      filename: filename
    }).toString();

    return knex.raw(query);

  },
  all() {
    return knex('photos').select('filename', 'updated_at');
  },
  page(lastUpdated) {
    if(lastUpdated) {
        return knex('photos').where('updated_at', '<', lastUpdated).orderBy('updated_at', 'desc').limit(30);
    } else {
        return knex('photos').limit(30).orderBy('updated_at', 'desc');
    }
  },
  close() {
      knex.destroy();
  }
}
