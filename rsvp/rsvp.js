const knex = require('knex')(require('./knexfile'))

module.exports = {
  rsvp ({ attending, email, names, guest_count, dietary_restrictions, suggestions }) {
    var query = knex('rsvp').insert({
      attending: attending,
      email: email,
      names: names,
      guest_count: guest_count,
      dietary_restrictions: dietary_restrictions,
      suggestions: suggestions
    }).toString();

    query += ' on duplicate key update ' + knex.raw('attending = ?, names = ?, guest_count = ?, dietary_restrictions = ?, suggestions = ?, updated_at = ?', [attending, names, guest_count, dietary_restrictions, suggestions, new Date()]).toString();
    return knex.raw(query);

  },
  report() {
    return knex('rsvp').select('attending', 'email', 'names', 'guest_count', 'dietary_restrictions', 'suggestions', 'updated_at');
  },
  close() {
      knex.destroy();
  }
}
