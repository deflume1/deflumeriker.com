const rsvp = require('./rsvp');
const nodemailer = require('nodemailer');
const poolConfig = {
    pool: true,
    host: 'mail.deflumeri.com',
    port: 587,
    secure: false, // use TLS
    auth: {
        user: 'ghost@deflumeri.com',
        pass: ''
    }
};

const emailReport = function() {
  rsvp.report().then((resultSet) => {
    let numAttending = 0;
    let numDeclined = 0;
    resultSet.forEach(function(element) {
        if(element.attending) {
            numAttending += element.guest_count;
        } else {
            numDeclined += element.guest_count;
        }
    });
    const totalGuests = numAttending + numDeclined;
    const todaysDate = new Date().toLocaleString('en-US', { timeZone: 'America/New_York' });
    let report = '<!DOCTYPE HTML>' +
                    '<html>' +
                        '<head>' +
                            '<meta charset="UTF-8">' +
                            '<title>RSVP Report as of ' + todaysDate + '</title>' +
                        '</head>' +
                        '<body>' +
                            '<h1>RSVP Report as of ' + todaysDate + '</h1>' +
                            '<h2>Summary:</h2>' +
                            '<ul>' +
                                '<li>Responses: ' + resultSet.length + '</li>' +
                                '<li>Guest Count Responded: ' + totalGuests + '</li>' +
                                '<li>Attending: ' + numAttending + '</li>' +
                                '<li>Declined: ' + numDeclined + '</li>' +
                            '</ul>' +
                            '<h2>Detailed Results</h2>' +
                            '<table width="100%" border="1">' +
                                '<tr>' +
                                    '<th>Names</th>' +
                                    '<th>Attending</th>' +
                                    '<th>Email</th>' +
                                    '<th>Guest Count</th>' +
                                    '<th>Dietary Restrictions</th>' +
                                    '<th>Suggestions</th>' +
                                    '<th>Last Updated</th>' +
                                '</tr>';

            resultSet.forEach(function(element) {
              const attendingStr = element.attending ? 'Yes' : 'No';
              const displayTimeStr = new Date(element.updated_at).toLocaleString('en-US', { timeZone: 'America/New_York' });
              report = report + '<tr>' +
                                    '<td>' + element.names + '</td>' +
                                    '<td>' + attendingStr + '</td>' + 
                                    '<td>' + element.email + '</td>' +
                                    '<td>' + element.guest_count + '</td>' +
                                    '<td>' + element.dietary_restrictions + '</td>' +
                                    '<td>' + element.suggestions + '</td>' +
                                    '<td>' + displayTimeStr + '</td>' +
                                '</tr>';
            });
       report = report + '</table>' +
                    '</body>' +
               '</html>';
    
       const mailOptions = {
            from: '"Katie and Chris" <ghost@deflumeri.com>', // sender address
            to: "chris@deflumeri.com,katherineriker@gmail.com",
            subject: 'Daily RSVP Report', // Subject line
            text:  JSON.stringify(resultSet),
            html: report
        };

        const transporter = nodemailer.createTransport(poolConfig);
        transporter.sendMail(mailOptions, (error, info) => {
            if(error) {
                console.log(error);
                process.exit(1);
            }
            console.log('Message sent: %s', info.messageId);
            process.exit(0); 
        });
    });
}

emailReport();
