exports.up = function(knex, Promise) {
    return knex.schema.createTable('photos', function (t) {
        t.string('filename').primary()
        t.timestamps(false, true)
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('photos')
};
