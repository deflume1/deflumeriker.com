
exports.up = function(knex, Promise) {
    return knex.schema.createTable('rsvp', function (t) {
        t.boolean('attending').notNullable()
        t.string('email').primary()
        t.string('names').notNullable()
        t.integer('guest_count').notNullable()
        t.text('dietary_restrictions')
        t.text('suggestions')
        t.timestamps(false, true)
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('rsvp')
};
