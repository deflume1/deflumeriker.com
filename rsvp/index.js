const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const app = express();
const rsvp = require('./rsvp');
const nodemailer = require('nodemailer');
const aws = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');
const cors = require('cors');
const spacesEndpoint = new aws.Endpoint('nyc3.digitaloceanspaces.com');
const s3 = new aws.S3({
  endpoint: spacesEndpoint
});

const poolConfig = {
    pool: true,
    host: 'mail.deflumeri.com',
    port: 587,
    secure: false, // use TLS
    auth: {
        user: '',
        pass: ''
    }
};

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(cookieParser());
app.use(cors());
app.use('/', express.static('static',{index:false,extensions:['html']}));

app.post('/rsvp', function(req, res){
    const attending = req.body.attending;
    const email = req.body.email;
    const names = req.body.names;
    const guest_count = req.body.guest_count;
    const dietary_restrictions = req.body.dietary_restrictions;
    const suggestions = req.body.suggestions;

    if(!req.cookies.loggedin) {
        res.sendStatus(403);
    } else if(typeof(attending) === 'undefined' || !email || email.trim() === '' || !names || names.length === 0) {
        res.sendStatus(500);
    } else { 
        rsvp.rsvp({
          attending: attending,
          email: email.trim().toLowerCase(),
          names: names.join(','),
          guest_count: guest_count,
          dietary_restrictions: dietary_restrictions.trim(),
          suggestions: suggestions.trim()
        })
        .then(() => {
            res.sendStatus(200);
            // setup email data with unicode symbols
            const attendingStr = attending ? "Yes!" : "No...";
            let firstNames = [];
            names.forEach(function(name) {
                const spaceSplit = name.trim().split(' ');
                firstNames.push(spaceSplit[0].trim());
            });

            const displayName = firstNames.length > 1 ? firstNames.join(' & ') : firstNames[0];
            const mailOptions = {
                from: '"Katie and Chris" <ghost@deflumeri.com>', // sender address
                replyTo: "chris@deflumeri.com, katherineriker@gmail.com",
                to: email.trim(), // list of receivers
                cc: "chris@deflumeri.com,katherineriker@gmail.com",
                subject: 'Thanks for RSVP\'ing, ' + displayName + '!', // Subject line
                text:  displayName +  ', thank you for RSVP\'ing at deflumeriker.com!  If you need to change your choices, just go back and submit the form again.  We\'ll use the latest submission. Love, Katie and Chris.', // plain text body
                html: '<!DOCTYPE HTML>' +
                        '<html>' + 
                            '<head>' +
                                '<meta charset="UTF-8">' +
                                '<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Space+Mono:400,700" rel="stylesheet">' +
                                '<style> body { font-family: "Space Mono", monospace; color: #122621 } ul {list-style-type: none;} img { width: 50%}</style>' +
                            '</head>' +    
                            '<body>' +
                                '<img src="https://deflumeriker.com/banner.png" alt="Katie and Chris">' +
                                '<h1>You\'re the best, ' + displayName +'!</h1>' + 
                                '<p>Thank you for RSVP\'ing at <a href="https://deflumeriker.com/rsvp">deflumeriker.com</a>!  If you need to change your choices, just go back and submit the <a href="https://deflumeriker.com/rsvp">form again</a>.  We\'ll use the latest submission.</p>' + 
                                '<h3>Your choices were:</h3>' +
                                '<ul>' +
                                    '<li> Attending: ' + attendingStr + '</li>' +
                                    '<li> Email: ' + email.trim().toLowerCase() + '</li>' +
                                    '<li> Names: ' + names.join(', ') + '</li>' +
                                    '<li> Guest Count: ' + guest_count + '</li>' +
                                    '<li> Dietary Restrictions: ' + dietary_restrictions + '</li>' +
                                    '<li> Suggestions: ' + suggestions + '</li>' +
                                '</ul>' +
                                '<h2>Love,</h2><h2>Katie and Chris</h2>' +
                            '</body>' +
                         '</html>'
            }; 

            const transporter = nodemailer.createTransport(poolConfig);
            transporter.sendMail(mailOptions, (error, info) => {
                if(error) {
                    return console.log(error);
                }
                console.log('Message sent: %s', info.messageId);
            });
        });
    }
});

app.get('/report', function(req, res) {
      if(!req.cookies.loggedin) {
        res.sendStatus(403);
      }
      rsvp.report().then((resultSet) => {
        let numAttending = 0;
        let numDeclined = 0;
        resultSet.forEach(function(element) {
            if(element.attending) {
                numAttending += element.guest_count;
            } else {
                numDeclined += element.guest_count;
            }
        });
        const totalGuests = numAttending + numDeclined;
        const todaysDate = new Date().toLocaleString('en-US', { timeZone: 'America/New_York' });
        let report = '<!DOCTYPE HTML>' +
                        '<html>' +
                            '<head>' +
                                '<meta charset="UTF-8">' +
                                '<title>RSVP Report as of ' + todaysDate + '</title>' +
                            '</head>' +
                            '<body>' +
                                '<h1>RSVP Report as of ' + todaysDate + '</h1>' +
                                '<h2>Summary:</h2>' +
                                '<ul>' +
                                    '<li>Responses: ' + resultSet.length + '</li>' +
                                    '<li>Guest Count Responded: ' + totalGuests + '</li>' +
                                    '<li>Attending: ' + numAttending + '</li>' +
                                    '<li>Declined: ' + numDeclined + '</li>' +
                                '</ul>' +
                                '<h2>Detailed Results</h2>' +
                                '<table width="100%" border="1">' +
                                    '<tr>' +
                                        '<th>Names</th>' +
                                        '<th>Attending</th>' +
                                        '<th>Email</th>' +
                                        '<th>Guest Count</th>' +
                                        '<th>Dietary Restrictions</th>' +
                                        '<th>Suggestions</th>' +
                                        '<th>Last Updated</th>' +
                                    '</tr>';

                resultSet.forEach(function(element) {
                  const attendingStr = element.attending ? 'Yes' : 'No';
                  const displayTimeStr = new Date(element.updated_at).toLocaleString('en-US', { timeZone: 'America/New_York' });
                  report = report + '<tr>' +
                                        '<td>' + element.names + '</td>' +
                                        '<td>' + attendingStr + '</td>' + 
                                        '<td>' + element.email + '</td>' +
                                        '<td>' + element.guest_count + '</td>' +
                                        '<td>' + element.dietary_restrictions + '</td>' +
                                        '<td>' + element.suggestions + '</td>' +
                                        '<td>' + displayTimeStr + '</td>' +
                                    '</tr>';
        });
           report = report + '</table>' +
                        '</body>' +
                   '</html>';
        
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/html');
        res.write(report);
        res.end();
      });
});

const upload = multer({
  storage: multerS3({
    s3: s3,
    bucket: 'deflumeriker',
    acl: 'public-read',
    key: function (request, file, cb) {
      cb(null, file.originalname);
    }
  })
}).array('file', 1);

app.post('/uploadPhotos', (req, res) => {
  upload(req, res, function (error) {
    if (error) {
      console.log(error);
    }
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.end("{}");
  });
});

const port = 38000;
app.listen(port);
console.log('Listening at http://localhost:' + port);
