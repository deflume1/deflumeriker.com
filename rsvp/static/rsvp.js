(function($) {
    "use strict";
    var errorMessage = "Chris is a bad programmer send him an angry email. ";
    var counter = 0;
    /**
     * Scroll the window to the element with id "<cssId>"
     *
     * @param cssId - a String that represents an element id on the page
     */
    var scrollToId = function(cssId) {
        if(!cssId || !(typeof cssId === 'string')) {
            alert(errorMessage + "cssId is undefined or not a string");
            return;
        }
        if(!cssId.charAt(0) === '#') {
            cssId = '#' + cssId;
        }

        var tag = $(cssId);
        if(tag.length === 0) {
            alert(errorMessage + "No tag for id " + cssId + ". Will not scroll.");
            return;
        }

        $('html,body').animate({scrollTop: tag.offset().top}, 300);
    };
    
    /**
     * Called when the + More button is clicked, adds another form element for name entry
     *
     * @param moreElem - The + More button that was clicked
     */
    var addMoreNames = function(moreElem) {
        counter++;
        moreElem.before('<div class="namesWrap"><input class="names" id="names' + counter + '" name="names[]" type="text"><label class="xbutton" for="names' + counter + '">X</label></div>');
       
        $('.xbutton').off();
        $('.xbutton').on('click', function(e) {
            xButton($(this));
            e.preventDefault();
            return false;
        });
    };

    /**
     * Called when the X button is clicked, removes the parent element from the DOM
     * 
     * @param xButton - The X button that was clicked
     */
    var xButton = function(xButtonLabel) {
        var elem = xButtonLabel.parent();
        elem.remove();
    };
    
    /**
     * Get the filtered array of names inputs
     *
     */
    var getNames = function() {
        return $.find('input[name^="names"]').map(function(elem, idx) {
             return $(elem).val();
        }).filter(function(name) { return name && name.trim().length > 0 })
    };

    /** 
     * Get the current names count
     */
    var nameCount = function() {
        return getNames().length;
    };

    /**
     * Called when the rsvp form is submitted.  Submits the email to the mailing list
     * API endpoint, then displays a success/failure message.
     * 
     * @param formElem - The jQuery wrapped form element that was submitted
     */
    var rsvpSubmit = function(formElem) {
        var attendingElem = formElem.find('input[name="attending"]:checked');
        var attending = attendingElem.val() === "true";
        var email = formElem.find('input[name="email"]').val();
        var names = getNames();

        var guest_count = names.length;
        var dietary_restrictions = formElem.find('textarea[name="dietary_restrictions"]').val();
        var suggestions = formElem.find('textarea[name="suggestions"]').val();
        
        var failed = false;

        if(!attendingElem || attendingElem.length === 0) {
            if($('.attending').parent().children('.form_error').length === 0) {
                $('.attending').parent().prepend("<div class='form_error'>Please click Yes or No!</div>");
            }
            failed = true;
        }

        if(!email || email === '') {
            if($('.email').parent().children('.form_error').length === 0) {
                $('.email').parent().prepend("<div class='form_error'>Please enter a valid email!</div>");
            }
            failed = true;
        }

        if(!names || names.length === 0 ) {
            if($('.names').parent().children('.form_error').length === 0) {
                $('.names').parent().prepend("<div class='form_error'>Please enter at least one name!</div>");
            }
            failed = true;
        }

        if(!guest_count || guest_count === 0) {
            if($('.guest_count').parent().children('.form_error').length === 0) {
                $('.guest_count').parent().prepend("<div class='form_error'>Please enter at least one guest!</div>");
            }
            failed = true;
        }

        if(failed) {
            scrollToId('#rsvp');
            return;
        }

        formElem.replaceWith("<h2 class='success'>Thanks for responding! The wedding already happened, but we hope you enjoyed filling out this form.</h2>");
        scrollToId('#hero-img');
        /*
        $.ajax({
            type: "POST",
            url: "/rsvp",
            data: JSON.stringify(
                { "attending" : attending,
                  "email" : email,
                  "names" : names,
                  "guest_count" : guest_count,
                  "dietary_restrictions" : dietary_restrictions,
                  "suggestions" : suggestions
                }
            ),
            success: function(data, statusCode, xhr) {
                formElem.replaceWith("<h2 class='success'>Thanks for responding!</h2>");
                scrollToId('#hero-img');
            },
            error: function(data, statusCode, xhr) {
                formElem.replaceWith("<h3 class='error'>Something went wrong!  Please reload and try again, or email chris@deflumeri.com .</h3>");
                scrollToId('#hero-img');
            },
            contentType: 'application/json'
        });
        */
        return false;
    };

    /**
     * Called when the login form is submitted.  
     * 
     * @param formElem - The jQuery wrapped form element that was submitted
     */
    var loginSubmit = function(formElem) {
        var loginVal = formElem.find('input[name="accesscode"]').val();

        if(loginVal && loginVal.toLowerCase() === 'swordfish') { // BEST IN CLASS SECURITY
            document.cookie = "loggedin=true; expires=Tue, 19 Jan 2038 03:14:07 UTC;";
            $('.modal-content').remove();
            $('.whole-content').removeClass('hidden');
            $('.modal').fadeTo(500, 0, function() { $('.modal').remove(); });
        } else {
            if($('.modal-content .error').length == 0) {
                $('.modal-content').append("<div class='error'>Sorry, that's not the right access code!  Please check your Save The Date!</div>");
            }
        }      
    };
    
    /**
     * Called on pageload to check if we're logged in.
     * If logged in, fade to page content
     * If not logged in, then display modal login dialog
     */
    var loginCheck = function() {
        if (document.cookie.split(';').filter(function(item) {
            return item.indexOf('loggedin=true') >= 0
            }).length) {
            $('.whole-content').removeClass('hidden');
            $('.modal-content').remove();
            $('.modal').remove();
            $('.whole-content').removeClass('hidden');
        } else {
            $('.modal').removeClass('hidden');
            $('.modal-content').removeClass('hidden');
        }
    };    
   
    /**
     * Clear the login cookie and refresh the page
     */
    var logout = function() {
        document.cookie = "loggedin=false; Max-Age=-9999999;";
        window.location.href = "https://deflumeriker.com/";
        return false;
    };
 
    // bind login form submit
    $('#sitelogin').on('submit', function(e) {
        loginSubmit($(this));
        e.preventDefault();
        return false;
    });
    
    // bind logout link
    $('#logout').on('click', function(e) {
        logout();
        e.preventDefault();
        return false;
    });
    
    // bind more button click
    $('.rsvp-form .more').on('click', function(e) {
        addMoreNames($(this));
        e.preventDefault();
        return false;
    });

    // bind rsvp form submit
    $('#rsvp').on('submit', function(e) {
        rsvpSubmit($(this));
        e.preventDefault();
        return false;
    });

    loginCheck();
    $(document).ready(function() {
        var screenHeight = $(window).height() / 2;
        $('#hero-img').css('height', screenHeight + 'px');
        $('input[name="guest_count"]').val(nameCount());
    });
})($);  
