(function($) {
    "use strict";
    var errorMessage = "Chris is a bad programmer send him an angry email. ";
    var weddingDate = new Date(Date.UTC(2018, 8, 29, 21));
    var msInOneDay = 1000*60*60*24;
    var msInOneHour = 1000*60*60;    
    
    /**
     * Return the number of days between two JavaScript dates
     * @param date1 - The start date
     * @param date2 - The end date
     * @return The number of days between the dates.  Positive days indicate 
     *         date1 > date2, negative indicate date1 < date2
     */
    var getDaysBetween = function(date1, date2) {
        return Math.round((date1.getTime() - date2.getTime())/msInOneDay); 
    };

    /**
     * Return the number of hours between two JavaScript dates
     * @param date1 - The start date
     * @param date2 - The end date
     * @return The number of hours between the dates.  Positive hours indicate 
     *         date1 < date2, negative indicate date1 > date2     
     */
    var getHoursBetween = function(date1, date2) {
        return Math.round((date1.getTime() - date2.getTime())/msInOneHour); 
    };        

    /**
     * 
     * Given a jQuery element, replace it with text indicating whether someone
     * is late to the wedding.
     
     * @param elem a jQuery element
     */
    var amILate = function(elem) {
        if(!elem || elem.length === 0) {
            alert(errorMessage + "No valid elem passed to function amILate.");
        }

        var today = new Date(Date.now());
        var daysBetween = getDaysBetween(weddingDate, today);

        if(daysBetween > 0) {
            if(daysBetween === 1) {
                elem.replaceWith("<span><span style='color:90ee90;'>Nope!</span> You have at least " + daysBetween + " day <a href='https://www.arekatieandchrismarriedyet.com/'>until</a> the <a href='https://arekatieandchrisgettingmarried.today/'>wedding</a>!</span>");
            } else {
                elem.replaceWith("<span><span style='color:90ee90;'>Nope!</span> You have at least " + daysBetween + "  days <a href='https://www.arekatieandchrismarriedyet.com/'>until</a> the <a href='https://arekatieandchrisgettingmarried.today/'>wedding</a>!</span>");
            }
        } else if(daysBetween === 0) {
            var hoursBetween = getHoursBetween(weddingDate, today);
            if(hoursBetween > 0) {            
                if(hoursBetween === 1) {
                    elem.replaceWith("<span><span style='color:orange;'>Possibly!</span> You have (approximately) " + hoursBetween + " hour <a href='https://www.arekatieandchrismarriedyet.com/'>until</a> the <a href='https://arekatieandchrisgettingmarried.today/'>wedding</a>!</span>");
                } else {
                    elem.replaceWith("<span><span style='color:orange;'>Possibly!</span> You have (approximately) " + hoursBetween + " hours <a href='https://www.arekatieandchrismarriedyet.com/'> the <a href='https://arekatieandchrisgettingmarried.today/'>wedding</a>!</span>");
                }
            } else {
                var positiveHours = -1 * hoursBetween;        
                if(positiveHours === 1) {
                    elem.replaceWith("<span><span style='color:red;'>Yep!</span> If you didn't already attend the <a href='https://arekatieandchrisgettingmarried.today/'>wedding</a>, it <a href='https://www.arekatieandchrismarriedyet.com/'>happened</a> at least " + positiveHours + " hour ago!</span>");
                } else {
                    elem.replaceWith("<span><span style='color:red;'>Yep!</span> If you didn't already attend the <a href='https://arekatieandchrisgettingmarried.today/'>wedding</a>, it <a href='https://www.arekatieandchrismarriedyet.com/'>happened</a> at least " + positiveHours + " hours ago!</span>");
                }
            }
        } else {
            var positiveDays = -1 * daysBetween;
            if(positiveDays === 1) {
                elem.replaceWith("<span><span style='color:red;'>Yep!</span> If you didn't already attend the <a href='https://arekatieandchrisgettingmarried.today/'>wedding</a>, it <a href='https://www.arekatieandchrismarriedyet.com/'>happened</a> at least " + positiveDays + " day ago!</span>");
            } else {
                elem.replaceWith("<span><span style='color:red;'>Yep!</span> If you didn't already attend the <a href='https://arekatieandchrisgettingmarried.today/'>wedding</a>, it <a href='https://www.arekatieandchrismarriedyet.com/'>happened</a> at least " + positiveDays + " days ago!</span>");
            }
        }
    };

    /**
     * Called when the mailing list form is submitted.  Submits the email to the mailing list
     * API endpoint, then displays a success/failure message.
     * 
     * @param formElem - The jQuery wrapped form element that was submitted
     */
    var mailingListSubmit = function(formElem) {
        var emailVal = formElem.find('input[name="email"]').val();
        $.ajax({
            type: "POST",
            url: "https://deflumeri.com/submitfornewsletter",
            data: JSON.stringify({ "email" : emailVal }),
            success: function(data, statusCode, xhr) {
                formElem.replaceWith("<span>You're on the list!</span>");
            },
            error: function(data, statusCode, xhr) {
                formElem.replaceWith("<span>Something went wrong!  You are not on the list.  Get out.</span>");
            },
            contentType: 'application/json'
        }); 
    };

    /**
     * Called when the login form is submitted.  
     * 
     * @param formElem - The jQuery wrapped form element that was submitted
     */
    var loginSubmit = function(formElem) {
        var loginVal = formElem.find('input[name="accesscode"]').val();

        if(loginVal && loginVal.toLowerCase() === 'swordfish') { // BEST IN CLASS SECURITY
            document.cookie = "loggedin=true; expires=Tue, 19 Jan 2038 03:14:07 UTC;";
            $('.modal-content').remove();
            $('.whole-content').removeClass('hidden');
            $('.modal').fadeTo(500, 0, function() { $('.modal').remove(); });
        } else {
            if($('.modal-content .error').length == 0) {
                $('.modal-content').append("<div class='error'>Sorry, that's not the right access code!  Please check your Save The Date!</div>");
            }
        }      
    };
    
    /**
     * Called on pageload to check if we're logged in.
     * If logged in, fade to page content
     * If not logged in, then display modal login dialog
     */
    var loginCheck = function() {
        if (document.cookie.split(';').filter(function(item) {
            return item.indexOf('loggedin=true') >= 0
            }).length) {
            $('.whole-content').removeClass('hidden');
            $('.modal-content').remove();
            $('.modal').remove();
            $('.whole-content').removeClass('hidden');
        } else {
            $('.modal').removeClass('hidden');
            $('.modal-content').removeClass('hidden');
        }
    };
   
    /**
     * Clear the login cookie and refresh the page
     */
    var logout = function() {
        document.cookie = "loggedin=false; Max-Age=-9999999;";
        window.location.href = "https://deflumeriker.com/";
        return false;
    };

    /**
     * Scroll the window to the element with id "<cssId>"
     *
     * @param cssId - a String that represents an element id on the page
     */
    var scrollToId = function(cssId) {
        if(!cssId || !(typeof cssId === 'string')) {
            alert(errorMessage + "cssId is undefined or not a string");
            return;
        }
        if(!cssId.charAt(0) === '#') {
            cssId = '#' + cssId;
        }

        var tag = $(cssId);
        if(tag.length === 0) {
            alert(errorMessage + "No tag for id " + cssId + ". Will not scroll.");
            return;
        }

        $('html,body').animate({scrollTop: tag.offset().top}, 300);
    };

    /**
     * Add a click handler for a given label.  Expects that elements that are bound
     * will have a CSS class like:
     *     <div class="<label> whatevercssclass"</div>
     * and that targets will have an element id like:
     *     <div id="<label>"></div>
     *
     * @param label - a String that binds an element with CSS class "<label>" to
     *                an element with id "<label>"
     */
    var addScrollClickHandler = function(label) {
        if(!label || !(typeof label === 'string')) {
            alert(errorMessage + "label is undefined or not a string");
            return;
        }

        var elements = $('.' + label);
        if(elements.length === 0) {
            alert(errorMessage + "No elements with CSS class " + label + ".");
            return;
        }

        $('.' + label).on('click', function(e) {
            scrollToId('#' + label);
            e.preventDefault();
            return false;
        });
    }
    
    // bind click handlers for nav
    addScrollClickHandler('wheretostay');
    addScrollClickHandler('rsvp');
    addScrollClickHandler('registry');
    addScrollClickHandler('faqs');  

    // bind Am I late?
    $('.datespan').on('click', function(e) {
        amILate($(this));
        e.preventDefault();
        return false;
    });

    // bind email form submit
    $('#emailsignup').on('submit', function(e) {
        mailingListSubmit($(this));
        e.preventDefault();
        return false;
    });

    // bind login form submit
    $('#sitelogin').on('submit', function(e) {
        loginSubmit($(this));
        e.preventDefault();
        return false;
    });
    
    // bind logout link
    $('#logout').on('click', function(e) {
        logout();
        e.preventDefault();
        return false;
    });

    loginCheck();
    $(document).ready(function() {
        var screenHeight = $(window).height();
        $('#hero-img').css('height', screenHeight + 'px');
    });
})($);  
