# The deflumeriker.com webapp

* app - contains the React app that powers the day-of wedding mobile app
* rsvp - contains the Node/expressJS app that takes image uploads and rsvp submissions
* public - static website route
* system - contains an nginx config that's part of the nginx config for the server that powers the main site

Not intended for re-use, just for people who are curious, hence no license