export const CHANGE_PAGE = 'page/CHANGE';

export const CHANGE_ZELDA = 'zelda/CHANGE';

export const UPLOAD_PHOTOS = 'photos/UPLOAD';

export const SHOW_UPLOAD_DONE_MESSAGE = 'photos/UPLOAD_DONE';
