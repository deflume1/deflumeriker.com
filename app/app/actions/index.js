import { CHANGE_PAGE, CHANGE_ZELDA, UPLOAD_PHOTOS, SHOW_UPLOAD_DONE_MESSAGE } from './actionTypes';

export function changePage(destinationPage) {
  return {
    type: CHANGE_PAGE,
    page: destinationPage
  };
};

export function changeZelda(zelda) {
  return {  
    type: CHANGE_ZELDA,
    currentZelda: zelda
  };
};

export function uploadPhotos(uploading) {
  return {  
    type: UPLOAD_PHOTOS,
    uploading: uploading
  };
};


export function showUploadDoneMessage(show) {
  return {  
    type: SHOW_UPLOAD_DONE_MESSAGE,
    showUploadDoneMessage: show
  };
};

