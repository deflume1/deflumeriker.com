import React from 'react';
import PropTypes from 'prop-types';
import { changePage } from '../actions';
import Link from 'redux-first-router-link'

const Button = props =>
  <div className="button">
      <Link to={"/" + props.destinationPage}>{props.label}</Link>
  </div>;

Button.propTypes = {
  label: PropTypes.string.isRequired,
  destinationPage: PropTypes.string.isRequired,
};

export default Button;
