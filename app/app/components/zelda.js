import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Toolbar from './toolbar';
import { changeZelda }from '../actions/index.js';

const getRandomZelda = () => {
  const id = Math.floor(Math.random() * 36) + 1;
  return "https://cdn.deflumeri.com/zelda/zelda" + id + ".jpg";
}

const Zelda = props =>
  <div>
    <Toolbar />
    <div className="main">
      <div className="zelda">
        <h1>She's Fine</h1>
      </div>
      <div className="photos">
         <img width="300px" height="300px" src={props.currentZelda} onClick={props.onClick}/>
      </div>
      <div className="tap">
        <p>(Tap Zelda For More Zelda)</p>
      </div>
    </div>
  </div>;

const mapStateToProps = (state, ownProps) => {
  return {
    page: state.page,
    currentZelda : state.currentZelda
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onClick: () => {
      dispatch(changeZelda(getRandomZelda()))
    }
  }
}

Zelda.propTypes = {
    page: PropTypes.string.isRequired,
    currentZelda: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Zelda);
