import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Toolbar from './toolbar';

const Ceremony = props =>
  <div>
    <Toolbar />
    <div className="main">
      <div className="ceremony">
        <h3>South Plaza Park</h3>
      </div>
      <div className="date">
        <h4>September 29th, 2018</h4>
      </div>
      <div className="time">
        <h4>5:00 P.M.</h4>
      </div>
      <div className="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d94336.76875810594!2d-71.15264058748448!3d42.36332728580995!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89e370a5ab67e701%3A0x60f092ace2e11a37!2s11+Broad+Canal+Way%2C+Cambridge%2C+MA+02142!5e0!3m2!1sen!2sus!4v1537232274043" width="300" height="300" frameBorder="0" style={{border:0}} allowFullScreen></iframe>
      </div>
      <div className="lyft">
        <a className="lyftButton" href="https://lyft.com/ride?id=lyft&pickup[latitude]=42.3624518&pickup[longitude]=-71.0862478&destination[latitude]=42.3630454&destination[longitude]=-71.0822398&partner=RAGYAfv8KgQG" target="_blank">Get A Lyft</a>
      </div>
    </div>
  </div>;

const mapStateToProps = (state, ownProps) => {
  return {
    page: state.page
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
  }
}

Ceremony.propTypes = {
    page: PropTypes.string.isRequired    
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Ceremony);
