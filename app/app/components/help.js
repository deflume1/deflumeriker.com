import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Toolbar from './toolbar';

const Help = props =>
  <div>
    <Toolbar />
    <div className="title">
      <h1>Call/Text</h1>
    </div>
    <div className="number">
        <a href="tel:19086017060">Katie</a>
        <a href="tel:15084045449">Chris</a>
    </div>
    <h1>Email</h1>
    <div className="email">
      <a href="mailto:katherineriker@gmail.com">Katie</a>
      <a href="mailto:chris@deflumeri.com">Chris</a>
    </div>
  </div>;

const mapStateToProps = (state, ownProps) => {
  return {
    page: state.page
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
  }
}

Help.propTypes = {
    page: PropTypes.string.isRequired    
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Help);
