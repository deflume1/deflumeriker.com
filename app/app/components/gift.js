import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Toolbar from './toolbar';

const Gift = props =>
  <div>
    <Toolbar />
    <div className="main">
      <div className="zola">
        <a href="https://www.zola.com/registry/rikerdeflumeri" target="_blank">
            <img src="/app/zola.png" />
        </a>
      </div>
      <div className="williamssonoma">
        <a href="https://www.crateandbarrel.com/gift-registry/katie-riker-and-chris-deflumeri/r5831587" target="_blank">
            <img width="300px" src="/app/crateandbarrel.png" />
        </a>
      </div>
    </div>
  </div>;

const mapStateToProps = (state, ownProps) => {
  return {
    page: state.page
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
  }
}

Gift.propTypes = {
    page: PropTypes.string.isRequired    
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Gift);
