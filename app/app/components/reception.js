import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Toolbar from './toolbar';

const Reception = props =>
  <div>
    <Toolbar />
    <div className="main">
      <div className="reception">
        <h3>Caf&eacute; ArtScience</h3>
      </div>
      <div className="date">
        <h4>September 29th, 2018</h4>
      </div>
      <div className="time">
        <h4>(After The Ceremony, ~6:00pm)</h4>
      </div>
      <div className="map">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2947.9732815521193!2d-71.083218384755!3d42.364408679186575!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89e370a4dda275d7%3A0x69f3a109f4e818b0!2sArtScience+Culture+Lab+%26+Caf%C3%A9!5e0!3m2!1sen!2sus!4v1537232688702" width="300" height="300" frameBorder="0" style={{border:0}} allowFullScreen></iframe>
      </div>
      <div className="lyft">
        <a className="lyftButton" href="https://lyft.com/ride?id=lyft&pickup[latitude]=42.3624518&pickup[longitude]=-71.0862478&destination[latitude]=42.3643802&destination[longitude]=-71.0811995&partner=RAGYAfv8KgQG" target="_blank">Get A Lyft</a> 
      </div>
    </div>
  </div>;

const mapStateToProps = (state, ownProps) => {
  return {
    page: state.page
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
  }
}

Reception.propTypes = {
    page: PropTypes.string.isRequired    
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Reception);
