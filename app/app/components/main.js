import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { changePage } from '../actions';
import Button from './button';
import Photo from './photo';
import Toolbar from './toolbar';

const Main = props =>
  <div>
    <Toolbar hideBackButton />
    <div className="main">
      <div className="title">
        <h2>Katie &amp; Chris</h2>
      </div>
      <div className="date">
        <h5>September 29th, 2018</h5>
      </div>
      <div className="buttons">
        <Button label="Ceremony!?" destinationPage="ceremony" />
        <Button label="Reception!?" destinationPage="reception" />
        <Button label="How's Zelda Doing?" destinationPage="zelda" />
        <Button label="Damn I forgot a gift..." destinationPage="gift" />
        <Button label="Uhhhh brunch?" destinationPage="brunch" />
        <Button label="I need help!" destinationPage="help" />
        <Photo />
      </div>
      <div className="footer">
        &copy; 2018 Chris DeFlumeri & Katie Riker 
      </div>
    </div>
  </div>;

const mapStateToProps = (state, ownProps) => {
  return {
    page: state.page
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
  }
}

Main.propTypes = {
    page: PropTypes.string.isRequired    
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Main);
