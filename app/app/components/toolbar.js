import React from 'react';
import PropTypes from 'prop-types';
import Link from 'redux-first-router-link'

const Toolbar = props =>
  <div className="toolbar">
    {!props.hideBackButton && <Link to={"/app"}>Back</Link>}
    <span className="appTitle">DeFlumeriker Wedding 2018</span>
  </div>;

Toolbar.propTypes = {
    hideBackButton: PropTypes.bool
};


export default Toolbar;
