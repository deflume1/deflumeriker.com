import ReactDOM from 'react-dom';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { changePage } from '../actions';
import Main from './main';
import Ceremony from './ceremony';
import Reception from './reception';
import Zelda from './zelda';
import Gift from './gift';
import Brunch from './brunch';
import Help from './help';
import Photo from './photo';

const Primary = props =>
  <div>    
    {props.page == "APP" && <Main />}
    {props.page == "CEREMONY" && <Ceremony />}
    {props.page == "RECEPTION" && <Reception />}
    {props.page == "ZELDA" && <Zelda />}
    {props.page == "GIFT" && <Gift />}
    {props.page == "BRUNCH" && <Brunch />}
    {props.page == "HELP" && <Help />}
  </div>;

const mapStateToProps = (state, ownProps) => {
  return {
    page: state.page
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
  }
}

Primary.propTypes = {
    page: PropTypes.string.isRequired    
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Primary);
