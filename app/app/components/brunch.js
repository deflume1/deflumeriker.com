import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Toolbar from './toolbar';

const Brunch = props =>
  <div>
    <Toolbar />
    <div className="title">
      <h3>S&amp;S Deli</h3>
      <h4>September 30th, 2018</h4>
      <h4>10:30 A.M.</h4>
      <h4>Pay-For-Your-Own</h4>
    </div>
    <div className="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2947.5538958784773!2d-71.10205898475476!3d42.37334557918593!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89e3774c83f350df%3A0x563f65ff2a1524ed!2s1334+Cambridge+St%2C+Cambridge%2C+MA+02139!5e0!3m2!1sen!2sus!4v1537237542575" width="300" height="300" frameBorder="0" style={{border:0}} allowFullScreen></iframe>
    </div>
    <div className="lyft">
      <a className="lyftButton" href="https://lyft.com/ride?id=lyft&pickup[latitude]=42.3624518&pickup[longitude]=-71.0862478&destination[latitude]=42.3735766&destination[longitude]=-71.0998084&partner=RAGYAfv8KgQG" target="_blank">Get A Lyft</a>
    </div>
  </div>;

const mapStateToProps = (state, ownProps) => {
  return {
    page: state.page
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
  }
}

Brunch.propTypes = {
    page: PropTypes.string.isRequired    
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Brunch);
