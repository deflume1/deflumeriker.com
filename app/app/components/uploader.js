import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Toolbar from './toolbar';
import Dropzone from 'react-dropzone';
import { uploadPhotos }from '../actions/index.js';

let dropzoneRef;

const Photo = props =>
  <div>
    <Toolbar />
    <div className="title">
      <h1>Upload Photo</h1>
        <Dropzone ref={(node) => { dropzoneRef = node; }} onDrop={props.onDrop}>
            <p>Drop files here.</p>
        </Dropzone>
        <button type="button" onClick={() => { dropzoneRef.open() }}>
          Open File Dialog
        </button>
    </div>
  </div>;

const mapStateToProps = (state, ownProps) => {
  return {
    page: state.page
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onDrop: (acceptedFiles, rejectedFiles) => {
      dispatch(uploadPhotos(acceptedFiles))
    }
  }
}

Photo.propTypes = {
    page: PropTypes.string.isRequired    
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Photo);
