import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Toolbar from './toolbar';
import Dropzone from 'react-dropzone';
import { uploadPhotos, showUploadDoneMessage }from '../actions/index.js';
import axios, { post } from 'axios';

let dropzoneRef;

const Photo = props =>
  <div className="button">
    <Dropzone accept="image/jpeg, image/png" ref={(node) => { dropzoneRef = node; }} onDrop={props.onDrop} style={{display: "none"}} />
    <button className="button" type="button" disabled={props.uploading} onClick={() => { dropzoneRef.open() }}>
      {!props.uploading ? "Take Or Upload A Photo" : "Uploading..."}
    </button>
    <div className={"uploads_complete" + (props.showUploadDoneMessage ? " show" : "")}>Photos uploaded!</div>
  </div>;

const mapStateToProps = (state, ownProps) => {
  return {
    page: state.page,
    uploading: state.uploading,
    showUploadDoneMessage: state.showUploadDoneMessage
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onDrop: (acceptedFiles, rejectedFiles) => {
      dispatch(uploadPhotos(true));
      dispatch(uploadPhotos(false));
      dispatch(showUploadDoneMessage(true));
      setTimeout(function(){ dispatch(showUploadDoneMessage(false)) }, 3000);
      /*
      const uploaders = acceptedFiles.map(file => {
            const data = new FormData();
            const date = new Date();
            const filename = "wedding_" + date.getFullYear() + "-" + ("0" + date.getMonth()).slice(-2) + "-" + ("0" + date.getDate()).slice(-2) + "-" + ("0" + date.getHours()).slice(-2) + "-" + ("0" + date.getMinutes()).slice(-2) + "-" + ("0" + date.getSeconds()).slice(-2) + "-" + ("00" + date.getMilliseconds()).slice(-3) + "-" + Math.floor(Math.random() * 100000) + "." + file.name.split(".").pop();

            data.append('file', file, filename);

            return axios.post('https://deflumeriker.com/uploadPhotos', data)
                .then(function (response) {
                console.log("image uploaded successfully!");
            })
            .catch(function (error) {
                console.log("image upload failed.");
            });
        });
      axios.all(uploaders).then(() => {
          console.log("all images uploaded successfully");
          dispatch(uploadPhotos(false));
          dispatch(showUploadDoneMessage(true));
          setTimeout(function(){ dispatch(showUploadDoneMessage(false)) }, 3000);
      });
      */
    }
  }
}

Photo.propTypes = {
    page: PropTypes.string.isRequired,
    uploading: PropTypes.bool.isRequired,
    showUploadDoneMessage: PropTypes.bool.isRequired
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Photo);
