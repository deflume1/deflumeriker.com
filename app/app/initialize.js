import ReactDOM from 'react-dom';
import React from 'react';
import { Provider, connect } from 'react-redux';
import Primary from './components/primary';

import { connectRoutes } from 'redux-first-router'
import { combineReducers, createStore, applyMiddleware, compose } from 'redux'
import createHistory from 'history/createBrowserHistory'
import pageReducer from './reducers/pageReducer'
import zeldaReducer from './reducers/zeldaReducer'
import photosReducer from './reducers/photosReducer'
import uploadDoneReducer from './reducers/uploadDoneReducer'
import changePage from './actions'

const history = createHistory();

// THE WORK:
const routesMap = { 
  APP: '/app',     
  CEREMONY: '/ceremony',  
  RECEPTION: '/reception',
  ZELDA: '/zelda',
  GIFT: '/gift',
  BRUNCH: '/brunch',
  HELP: '/help'
}

const { reducer, middleware, enhancer } = connectRoutes(history, routesMap);

const rootReducer = combineReducers({ location: reducer, page: pageReducer, currentZelda: zeldaReducer, uploading: photosReducer, showUploadDoneMessage: uploadDoneReducer });

const middlewares = applyMiddleware(middleware);

const store = createStore(rootReducer, compose(enhancer, middlewares));

const mapStateToProps = ({ page }) => ({ page });

const mapDispatchToProps = (dispatch, ownProps) => ({
});

const PrimaryContainer = connect(mapStateToProps, mapDispatchToProps)(Primary)

const App = () =>
  <Provider store={store}>
    <PrimaryContainer />
  </Provider>;

ReactDOM.render(<App />, document.querySelector('#app'));
