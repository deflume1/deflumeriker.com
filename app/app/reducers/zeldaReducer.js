import { CHANGE_ZELDA } from '../actions/actionTypes';

export default function zeldaReducer(state = null, action = {}) {
  switch (action.type) {
    case CHANGE_ZELDA:
      return action.currentZelda;
    default:
      return "https://cdn.deflumeri.com/zelda/zelda23.jpg";
  }
}
