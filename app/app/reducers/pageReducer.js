import { NOT_FOUND } from 'redux-first-router';
import { CHANGE_ZELDA } from '../actions/actionTypes';

export default function pageReducer(state = null, action = {}) {
  switch (action.type) {
    case 'APP':
        return 'APP';
    case 'CEREMONY':
        return 'CEREMONY';
    case 'RECEPTION':
        return 'RECEPTION';
    case 'ZELDA':
        return 'ZELDA';
    case 'GIFT':
        return 'GIFT';
    case 'BRUNCH':
        return 'BRUNCH';
    case 'HELP':
        return 'HELP';
    case 'PHOTO':
        return 'PHOTO';
    case NOT_FOUND:
        return null;
    default:
        return state;    
  }
}
