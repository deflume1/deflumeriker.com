import { SHOW_UPLOAD_DONE_MESSAGE } from '../actions/actionTypes';

export default function uploadDoneReducer(state = null, action = {}) {
  switch (action.type) {
    case SHOW_UPLOAD_DONE_MESSAGE:
      return action.showUploadDoneMessage;
    default:
      return false;
  }
}
