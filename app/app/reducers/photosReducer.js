import { UPLOAD_PHOTOS } from '../actions/actionTypes';

export default function photosReducer(state = null, action = {}) {
  switch (action.type) {
    case UPLOAD_PHOTOS:
      return action.uploading;
    default:
      return false;
  }
}
