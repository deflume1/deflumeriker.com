/**
 * server.js - Express webserver and configuration
 *
 * @author cdeflumeri
 * @since 7/2018
 */
module.exports = function() {
    const express = require('express');
    const app = express();
    app.use('/', express.static('public',{index:false,extensions:['html']}));
    app.listen(43000, () => console.log('Application listening on port 43000'));
};
