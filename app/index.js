/**
 * index.js - The entry point for the deflumeriker.com webapp
 * 
 * @author cdeflumeri
 * @since 7/2018
 */
const server = require('./server/server.js');
server();
